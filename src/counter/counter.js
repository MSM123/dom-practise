var inc = document.getElementById('add');
inc.addEventListener('click',incFunc);

var dec=document.getElementById('sub');
dec.addEventListener('click',decFunc);

function incFunc() {
    var foo = document.getElementById('here').innerHTML;
    foo++;
    document.getElementById('here').innerHTML = foo;
}

function decFunc() {
    var foo = document.getElementById('here').innerHTML;
    foo--;
    document.getElementById('here').innerHTML = foo;
}


/*
function dontStopInc() {
window.setInterval(incFunc,10);
}
function dontStopDec() {
window.setInterval(decFunc,10);
}
inc.addEventListener('click',dontStopInc);
dec.addEventListener('click',dontStopDec);
*/