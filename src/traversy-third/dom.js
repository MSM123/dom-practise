var form = document.getElementById('addForm');
form.addEventListener('submit', addItem);
var itemList = document.getElementById('items');
itemList.addEventListener('click',removeItem);


function addItem(e) {
 e.preventDefault();
 var newItem=document.getElementById('item').value;
 
 if (newItem.trim() === '') {
    alert('Please enter text items');
    return 0;
  }
  document.getElementById('item').value='';
 var li= document.createElement('li');
 li.className='list-group-item';
li.appendChild(document.createTextNode(newItem));
var deleteBtn = document.createElement('button');
deleteBtn.className='btn btn-danger btn-sm float-right delete';
deleteBtn.appendChild(document.createTextNode('X'));
li.appendChild(deleteBtn);
itemList.appendChild(li);
}

function removeItem(e){
   if(e.target.classList.contains('delete')) {
       if(confirm('Are you sure?')) {
           var li = e.target.parentElement;
       itemList.removeChild(li);    
       }
   }
}
